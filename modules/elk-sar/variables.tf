variable "log_group" {
  type = string
}

variable "sar_config_file" {
  type    = string
  default = "sarconfig.yaml"
}

variable "bucket_id" {
  type = string
}

variable "name" {
  type = string
}

#Terraform cloud
variable "prefix" {
  type = string
}