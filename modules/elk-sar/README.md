# terraform-module

  

This module :

* Create a Lambda for monitoring of log groups CloudWatch in elastic.co

  

## Dependency

  

- [terraform](https://www.terraform.io/) >= 1.2.0

  

## Using

  

Create a file **monitoring.tf**  you can use examble below:

  

```terraform

  

module  "elk_sar" {
  source = "../../modules/elk-sar"
  log_group = var.log_groups
  sar_config_file = local.path_sarconfig
  bucket_id = var.bucket_id
  prefix = var.prefix
  name = var.name
}


```

  

## Inputs

| **Name** | **Description** | **Type** | **Default** | **Required** |  **Obs**
|------|-------------|:----:|:-----:|:-----:|:-------:|
| **log_group** |  Complente path log_group | string | no | yes |  Necessary values for: REGION,AWS_ACCOUNT_ID,PATH_LOG_GROUP
| **sar_config_file** | Name configuration file sarconfig.yaml| string | yes | no |  The file must exist in the bucket with the name sarconfig.yaml
| **bucket_id** | Bucket ID | string | no | yes |  


## Important
Demo file sarconfig.yaml > https://github.com/elastic/elastic-serverless-forwarder/blob/main/docs/README-AWS.md#s3_config_file