
# terraform-module

  

This module :

* Create EC2 Instance

  

## Dependency

  

- [terraform](https://www.terraform.io/) >= 1.2.0

  

## Using

  

Create a file **application.tf**  you can use examble below:

  

```terraform

  

module  "ec2" {
	source = "../modules/ec2-instance"
	prefix = var.prefix
	count = var.prefix == "prod" ? 1 : 0
	ami_instance = "ami-...."
	type_instance = "t2.medium"
	name_instance = "name"
	zone_instance = "sa-east-1a"
	key_name_instance = "name"
	vpc_security_group_ids_instance = "sg-...."
	subnet_id_instance = "subnet-..."
	name_departament = "name..."
}


```

  

## Inputs

| **Name** | **Description** | **Type** | **Default** | **Required** |  **Obs**
|------|-------------|:----:|:-----:|:-----:|:-------:|
| **ami_instance** |  ami ec2 | string | not | yes |  
| **type_instance** | type instance | string | not | yes |  
| **name_instance** | name instance | string | not | yes |  
| **zone_instance** | public key pair | string | not | yes |  
| **key_name_instance** | key pair name | string | not | yes | key pair existing  
| **vpc_security_group_ids_instance** | id security group | string | not | yes |   
| **subnet_id_instance** | subnet id | string | not | yes |  
| **name_departament** | departament | string | not | yes |  
