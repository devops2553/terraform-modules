resource "aws_instance" "power-bi" {
  ami                     = var.ami_instance
  instance_type           = var.type_instance
  availability_zone       = var.zone_instance
  disable_api_termination = var.termination_instance
  key_name                = var.key_name_instance
  vpc_security_group_ids  = [var.vpc_security_group_ids_instance]
  subnet_id               = var.subnet_id_instance

  tags = {
    Name   = var.name_instance
    EC2    = var.name_departament
    tostop = var.tostop_instance
  }

  #Copy correct public key for instance (bastion_key)
  user_data = <<EOF
#!/bin/bash
echo -e "${var.public_key_bastion}" >> /home/ubuntu/.ssh/authorized_keys

EOF
}   