variable "ami_instance" {
  type = string
}

variable "type_instance" {
  type = string
}

variable "name_instance" {
  type = string
}

variable "zone_instance" {
  type = string
}

variable "key_name_instance" {
  type = string
}

variable "vpc_security_group_ids_instance" {
  type = string
}

variable "subnet_id_instance" {
  type = string
}

variable "name_departament" {
  type = string
}

variable "prefix" {
  type = string
}

variable "tostop_instance" {
  type = string
}

variable "public_key_bastion" {
  type        = string
  sensitive   = true
  description = "Publick key for use in ec2 instances"
}

variable "termination_instance" {
  type    = bool
  default = true
}