output "s3_bucket_target" {
  description = "Target Bucket"
  value       = var.bucket_name_for_sftp
}

output "endpoint_sftp" {
  description = "Host For Access"
  value       = aws_transfer_server.transfer_sftp_server.endpoint
}

output "name_sftp" {
  description = "Name SFTP Server"
  value       = aws_transfer_server.transfer_sftp_server.tags
}

output "login_sftp" {
  value = var.transfer_user_name
}