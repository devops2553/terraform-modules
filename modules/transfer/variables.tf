variable "transfer_user_name" {
  description = "Name user SFTP"
}

variable "bucket_name_for_sftp" {
  description = "Name of Bucket to Connection"
}

variable "partner_folder" {
  description = "Folder Owner Partner"
}

#Terraform cloud
variable "prefix" {
  type = string
}

variable "sftp-public-key" {
  type = string
}