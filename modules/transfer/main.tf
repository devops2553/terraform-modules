data "aws_s3_bucket" "bucket_partner" {
  bucket = var.bucket_name_for_sftp
}

#Create a partner folder in bucket dev
resource "aws_s3_bucket_object" "partner_folder" {
  bucket       = data.aws_s3_bucket.bucket_partner.id
  key          = "${var.partner_folder}/"
  content_type = "application/x-directory"
}

#Retrieve ssh-public-key-pair from terraform cloud
resource "aws_transfer_ssh_key" "transfer_sftp_ssh_key" {
  server_id = aws_transfer_server.transfer_sftp_server.id
  user_name = aws_transfer_user.transfer_sftp_user.user_name
  body      = var.sftp-public-key
}

resource "aws_transfer_server" "transfer_sftp_server" {
  identity_provider_type = "SERVICE_MANAGED"
  security_policy_name   = "TransferSecurityPolicy-2020-06"

  tags = {
    NAME = "${var.prefix}-tf-acc-transfer-server-sftp-${var.partner_folder}"
  }

  depends_on = [
    resource.aws_s3_bucket_object.partner_folder
  ]

}

resource "aws_transfer_user" "transfer_sftp_user" {
  server_id = aws_transfer_server.transfer_sftp_server.id
  user_name = var.transfer_user_name
  role      = aws_iam_role.transfer_iam_role.arn

  home_directory_type = "LOGICAL"
  home_directory_mappings {
    entry  = "/"
    target = "/${var.bucket_name_for_sftp}/${var.partner_folder}"
  }

  tags = {
    NAME = var.transfer_user_name
  }
}

resource "aws_iam_role" "transfer_iam_role" {
  name = "${var.prefix}-tf-transfer-user-iam-role"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
        "Effect": "Allow",
        "Principal": {
            "Service": "transfer.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "transfer_iam_role_policy" {
  name = "${var.prefix}-tf-transfer-user-iam-policy"
  role = aws_iam_role.transfer_iam_role.id

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowListingOfUserFolder",
            "Action": [
                "s3:ListBucket"
            ],
            "Effect": "Allow",
            "Resource": [
                "arn:aws:s3:::${var.bucket_name_for_sftp}"
            ]
        },
        {
            "Sid": "HomeDirObjectAccess",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObject",
                "s3:DeleteObjectVersion",
                "s3:GetBucketLocation",
                "s3:GetObjectVersion",
                "s3:GetObjectACL",
                "s3:PutObjectACL"
            ],
            "Resource": "arn:aws:s3:::${var.bucket_name_for_sftp}/${var.partner_folder}*"
        }
    ]
}
POLICY
}