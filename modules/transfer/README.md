# terraform-module

This module :
* Create AWS Transfer Framily of type SFTP

## Dependency

 - [terraform](https://www.terraform.io/) >= 1.2.0

## Using

Create a file **transfer_partner.tf** in folder shared, you can use examble below:

```terraform

module "transfer_sftp" {
  source               = "../modules/transfer"
  prefix               = var.prefix
  partner_folder       = "qi-tech"
  sftp-public-key      = var.qi-tech-sftp-public-key
  count                = var.prefix == "prod" ? 1 : 0
  bucket_name_for_sftp = "${var.prefix}-company-singularity"
  transfer_user_name   = "${var.prefix}-qi-tech"
} 


#outputs.tf
#AWS Transfer
output "s3_bucket_target" {
  description = "Target Bucket"
  value       = module.transfer_sftp[0].s3_bucket_target
}

#AWS Transfer
output "endpoint_sftp" {
  description = "Host For Access"
  value       = module.transfer_sftp[0].endpoint_sftp
}

#AWS Transfer
output "name_sftp" {
  description = "Name SFTP Server"
  value       = module.transfer_sftp[0].name_sftp
}

#AWS Transfer
output "login_sftp" {
  value = module.transfer_sftp[0].login_sftp
}

#variables.tf
variable "qi-tech-sftp-public-key" {
  type        = string
  description = "Public key for qi-tech"
}
```

## Inputs

| **Name** | **Description** | **Type** | **Default** | **Required** |  **Obs**
|------|-------------|:----:|:-----:|:-----:|:-------:|
| **partner_folder** |  Name of folder sftp | string | not | yes | folder for new partner
| **transfer_user_name** | login name sftp | string | not | yes | follow default env-partner, ex. dev-qi-tech
| **bucket_name_for_sftp** | bucket name sftp | string | yes | no | default for each environment
| **partner-sftp-public-key** | public key pair | string | not | yes | create on terraform cloud

## Important!  
A entrada partner-sftp-public-key precisa ser criada apenas na nuvem do Terraform, esse nome padrão é: partner-sftp-public-key. Para um novo partner, criar alterando apenas o nome partner para o nome desejado, ex: qi-tech-sftp-public-key.  Seu valor deve ser uma chave pública ssh em todos os ambientes da Cloud do Terraform. 

# ToDo

- [ ] Add support other type AWS Transfer Family