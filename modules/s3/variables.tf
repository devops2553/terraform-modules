variable "name_bucket" {
  type    = string
  default = "test-"
}

variable "block_public_acls" {
  type    = bool
  default = true
}

variable "block_public_policy" {
  type    = bool
  default = true
}

variable "restrict_public_buckets" {
  type    = bool
  default = true
}

variable "ignore_public_acls" {
  type    = bool
  default = true
}


#Terraform cloud
variable "prefix" {
  type = string
}