resource "aws_s3_bucket" "bucket" {
  bucket = var.name_bucket
  tags = {
    Service     = "S3"
    Name        = "${var.name_bucket}"
    Environment = "${var.prefix}"
  }
}

resource "aws_s3_bucket_public_access_block" "buckle_block_all" {
  bucket = aws_s3_bucket.bucket.id

  block_public_acls       = var.block_public_acls
  block_public_policy     = var.block_public_policy
  restrict_public_buckets = var.restrict_public_buckets
  ignore_public_acls      = var.ignore_public_acls
}