# terraform-module

This module :
* Create a Bucket in AWS S3

## Dependency

 - [terraform](https://www.terraform.io/) >= 1.2.0

## Using

Create a file **my_bucket.tf** in your folder, you can use examble below:

```terraform

module "s3" {
  source = "../module/s3"
  name_bucket = "name_bucket"
  prefix = "dev"
}


#outputs.tf
output "bucket_regional_domain_name" {
    value = length(module.s3-test) > 0 ? module.s3-test[0].bucket_regional_domain_name : null
}

output "bucket_arn" {
    value = length(module.s3-test) > 0 ? module.s3-test[0].bucket_arn : null
}

output "bucket_id" {
    value =length(module.s3-test) > 0 ?  module.s3-test[0].bucket_id : null
}

output "bucket_region" {
    value = length(module.s3-test) > 0 ? module.s3-test[0].bucket_region : null
}

#variables.tf
variable "prefix" {
  type        = string
  description = "Prefix to give for some resources"
}

```

## Inputs

| **Name** | **Description** | **Type** | **Default** | **Required** |  **Obs**
|------|-------------|:----:|:-----:|:-----:|:-------:|
| **name_bucket** |  Name of bucket | string | not | yes | Unique Name your bucket in world


# ToDo

- [ ] Add support ACL
- [ ] Add support Versioning
- [ ] Add support encryption
- [ ] Add support objetc lock
